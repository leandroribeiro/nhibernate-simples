﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using c = System.Console;

namespace NHibernate.Simples.Console {
    class Program {
        static void Main(string[] args) {

            ISession session = NHibernateHelper.GetCurrentSession();
            IQuery query = session.CreateQuery("FROM Aluno");
            IList<Aluno> acc = query.List<Aluno>();
            session.Close();
            if (acc.Count == 0)
                c.WriteLine("Cannot find specified user");
            else
                c.WriteLine("Found " + acc[0].Nome);

            c.ReadKey();
        }
    }
}
