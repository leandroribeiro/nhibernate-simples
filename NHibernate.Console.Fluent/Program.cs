﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using c = System.Console;

namespace NHibernate.Console.Fluent {
    class Program {

        static void Main(string[] args) {

            var sessionFactory = CreateSessionFactory();

            using (var session = sessionFactory.OpenSession()) {
                using (var transaction = session.BeginTransaction()) {
                    var aluno = new Aluno() { Nome = "Leandro" };
                    session.SaveOrUpdate(aluno);
                    transaction.Commit();
                }

            }

            c.ReadKey();
        }

        private static ISessionFactory CreateSessionFactory() {
            return Fluently.Configure()
              .Database(
                MsSqlConfiguration
                .MsSql2008
                .ConnectionString(x => x.FromConnectionStringWithKey("GestaoDeAlunos"))
                .ShowSql()
                .FormatSql()
              )
              .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Program>()
              )
              .ExposeConfiguration(BuildSchema)
              .BuildSessionFactory();
        }

        private static void BuildSchema(Configuration config) {

            // this NHibernate tool takes a configuration (with mapping info in)
            // and exports a database schema from it
            new SchemaExport(config)
              .Create(false, true);
        }

    }
}
