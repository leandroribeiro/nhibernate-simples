﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace NHibernate.Console.Fluent {
    public class AlunoMap : ClassMap<Aluno> {

        public AlunoMap() {

            Table("Alunos");
            Id(x => x.Id);
            Map(x => x.Nome);
        }

    }
}
